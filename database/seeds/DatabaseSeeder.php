<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        factory(App\User::class, 20)->create();
        \App\User::create([
            'name' => 'Suson Waiba',
            'email' => 'susonwaiba@gmail.com',
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm'
        ]);

        // Adding relationship
        \App\Friend::create([
            'user_id' => 21,
            'friend_id' => 1,
            'status' => 1
        ]);
        \App\Friend::create([
            'friend_id' => 21,
            'user_id' => 1,
            'status' => 1
        ]);

        // Creating chat instance
        \App\Chat::create([
            'user_id' => 21,
            'friend_id' => 1,
            'last_message' => ''
        ]);


        factory(App\Message::class, 100)->create();
    }
}
