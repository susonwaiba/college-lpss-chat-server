<?php

use Faker\Generator as Faker;

$factory->define(\App\Message::class, function (Faker $faker) {
    return [
        'chat_id' => 1,
        'message' => $faker->text,
    ];
});
