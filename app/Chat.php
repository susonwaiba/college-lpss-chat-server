<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $guarded = array();

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function friend()
    {
        return $this->hasOne('App\User', 'id', 'friend_id');
    }
}
