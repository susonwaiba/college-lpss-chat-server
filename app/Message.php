<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = array();

    public function chat()
    {
        return $this->hasOne('App\Chat', 'id', 'chat_id');
    }
}
