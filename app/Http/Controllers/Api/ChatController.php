<?php

namespace App\Http\Controllers\Api;

use App\Chat;
use App\Friend;
use App\Message;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Chat as ChatResource;
use App\Http\Resources\Message as MessageResource;

class ChatController extends Controller
{
    public function chats(Request $request)
    {
        $chats = Chat::where('user_id', $request->user()->id)->orderBy('updated_at', 'desc')->get();
        return response()->json([
            'status' => 1,
            'data' => ChatResource::collection($chats),
            'data_count' => $chats->count()
        ]);
    }

    public function messages($friend_id, $last_id = 0, Request $request)
    {
        $friend = User::find($friend_id);
        if (!$friend) {
            return response()->json([
                'status' => 0,
                'message' => 'Error: User not found!'
            ], '404');
        }

        /*$relationship = Friend::where('user_id', $request->user()->id)->where('friend_id', $friend_id)->where('status', true)->first();
        if (!$relationship) {
            return response()->json([
                'status' => 0,
                'message' => 'Error: User is not your friend!'
            ], '404');
        }*/

        $chat = Chat::where('user_id', $request->user()->id)->where('friend_id', $friend_id)->first();
        if (!$chat) {
            return response()->json([
                'status' => 1,
                'data' => [],
                'data_count' => 0,
                'last_id' => (int) $last_id
            ]);
        }

//        $messages = MessageResource::collection(Message::where('chat_id', $chat->id)->orderBy('id', 'desc')->take(20)->get());
        $messages = Message::where('chat_id', $chat->id)->orderBy('id', 'desc')->where('id', '>', $last_id)->take(30)->get();

        $messages_array = $messages->toArray();
        sort($messages_array);

        $messages_count = $messages->count();

        if ($messages_count) {
            $last_id = $messages[0]->id;
        }

        return response()->json([
            'status' => 1,
            'data' => $messages_array,
            'data_count' => $messages_count,
            'last_id' => (int) $last_id
        ]);
    }

    public function addMessage($friend_id, Request $request)
    {
        $friend = User::find($friend_id);
        if (!$friend) {
            return response()->json([
                'status' => 0,
                'message' => 'Error: User not found!'
            ], '404');
        }

        /*$relationship = Friend::where('user_id', $request->user()->id)->where('friend_id', $friend_id)->where('status', true)->first();
        if (!$relationship) {
            return response()->json([
                'status' => 0,
                'message' => 'Error: User is not your friend!'
            ], '404');
        }*/

        $validator = Validator::make($request->all(), [
            'message' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 0,
                'message' => 'Something went wrong!',
                'error' => $validator->errors()
            ], 400);
        }

        $message = $request->input('message');

        $chat = Chat::where('user_id', $request->user()->id)->where('friend_id', $friend_id)->first();
        $alt_chat = Chat::where('friend_id', $request->user()->id)->where('user_id', $friend_id)->first();
        if (!$chat) {
            $chat = Chat::create([
                'user_id' => $request->user()->id,
                'friend_id' => $friend_id,
                'last_message' => $message
            ]);
        } else {
            $chat->update([
                'last_message' => $message
            ]);
        }
        if (!$alt_chat) {
            $alt_chat = Chat::create([
                'friend_id' => $request->user()->id,
                'user_id' => $friend_id,
                'last_message' => $message
            ]);
        } else {
            $alt_chat->update([
                'last_message' => $message
            ]);
        }

        Message::create([
            'chat_id' => $chat->id,
            'user_id' => Auth::user()->id,
            'message' => $message
        ]);
        Message::create([
            'chat_id' => $alt_chat->id,
            'user_id' => Auth::user()->id,
            'message' => $message
        ]);

        return response()->json([
            'status' => 1
        ]);
    }

    public function say_hi(Request $request)
    {
        $friend = User::where('email', $request->input('email'))->first();
        if (!$friend) {
            return response()->json([
                'status' => 0,
                'message' => 'Error: User not found!'
            ], '404');
        }

        /*$relationship = Friend::where('user_id', $request->user()->id)->where('friend_id', $friend_id)->where('status', true)->first();
        if (!$relationship) {
            return response()->json([
                'status' => 0,
                'message' => 'Error: User is not your friend!'
            ], '404');
        }*/

        $message = "Hi";
        $friend_id = $friend->id;

        $chat = Chat::where('user_id', $request->user()->id)->where('friend_id', $friend_id)->first();
        $alt_chat = Chat::where('friend_id', $request->user()->id)->where('user_id', $friend_id)->first();
        if (!$chat) {
            $chat = Chat::create([
                'user_id' => $request->user()->id,
                'friend_id' => $friend_id,
                'last_message' => $message
            ]);
        } else {
            $chat->update([
                'last_message' => $message
            ]);
        }
        if (!$alt_chat) {
            $alt_chat = Chat::create([
                'friend_id' => $request->user()->id,
                'user_id' => $friend_id,
                'last_message' => $message
            ]);
        } else {
            $alt_chat->update([
                'last_message' => $message
            ]);
        }

        Message::create([
            'chat_id' => $chat->id,
            'user_id' => Auth::user()->id,
            'message' => $message
        ]);
        Message::create([
            'chat_id' => $alt_chat->id,
            'user_id' => Auth::user()->id,
            'message' => $message
        ]);

        return response()->json([
            'status' => 1,
            'user_id' => (int) $friend->id,
            'user_name' => $friend->name
        ]);
    }
}
