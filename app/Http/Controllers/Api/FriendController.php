<?php

namespace App\Http\Controllers\Api;

use App\Chat;
use App\Friend;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Friend as FriendResource;
use App\Http\Resources\Request as RequestResource;

class FriendController extends Controller
{
    public function friends(Request $request)
    {
        $friends = Friend::where('user_id', $request->user()->id)->where('status', true)->get();
        return response()->json([
            'status' => 1,
            'data' => FriendResource::collection($friends),
            'data_count' => $friends->count()
        ]);
    }
    public function requests(Request $request)
    {
        $requests = Friend::where('friend_id', $request->user()->id)->where('status', false)->get();
        return response()->json([
            'status' => 1,
            'data' => RequestResource::collection($requests),
            'data_count' => $requests->count()
        ]);
    }

    public function addFriend($friend_id, Request $request)
    {
        $friend = User::find($friend_id);
        if (!$friend) {
            return response()->json([
                'status' => 0,
                'message' => 'Error: User not found!'
            ], '404');
        }

        // Friend request accepted
        $alt_relationship = Friend::where('friend_id', $request->user()->id)->where('user_id', $friend_id)->first();
        if ($alt_relationship) {
            $alt_relationship->update([
                'status' => true
            ]);
            Friend::create([
                'user_id' => $request->user()->id,
                'friend_id' => $friend_id,
                'status' => true
            ]);
            return response()->json([
                'status' => 1,
                'message' => 'Success: Request has been accepted!'
            ]);
        }

        $relationship = Friend::where('user_id', $request->user()->id)->where('friend_id', $friend_id)->first();
        if ($relationship) {
            return response()->json([
                'status' => 0,
                'message' => $relationship->status ? 'Error: You are already friends!' : 'Error: You have already requested!'
            ], '404');
        }

        Friend::create([
            'user_id' => $request->user()->id,
            'friend_id' => $friend_id
        ]);

        return response()->json([
            'status' => 1,
            'message' => 'Success: Request has been sent!'
        ]);
    }

    public function removeFriend($friend_id, Request $request)
    {
        $friend = User::find($friend_id);
        if (!$friend) {
            return response()->json([
                'status' => 0,
                'message' => 'Error: User not found!'
            ], '404');
        }

        $relationship = Friend::where('user_id', $request->user()->id)->where('friend_id', $friend_id)->first();
        if (!$relationship) {
            return response()->json([
                'status' => 0,
                'message' => 'Error: You are not related!'
            ], '404');
        }

        // Remove relationship
        $relationship->delete();
        Friend::where('user_id', $friend_id)->where('friend_id', $request->user()->id)->delete();

        // Remove chats and messages for user and friend
        $chat = Chat::where('user_id', $request->user()->id)->where('friend_id', $friend_id)->first();
        Message::where('chat_id', $chat->id)->delete();
        $chat->delete();

        $alt_chat = Chat::where('friend_id', $request->user()->id)->where('user_id', $friend_id)->first();
        Message::where('chat_id', $alt_chat->id)->delete();
        $alt_chat->delete();

        return response()->json([
            'status' => 1,
            'message' => $relationship->status ? 'Success: Friend has been removed!' : 'Success: Request has been removed!'
        ]);
    }
}
