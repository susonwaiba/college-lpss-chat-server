<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 0,
                'message' => 'Something went wrong!',
                'error' => $validator->errors()->first()
            ], 400);
        }

        $email = $request->email; // $_POST['email'];
        $password = $request->password;

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // SQL: $pdo->query("select * from `users` where `email`='$email'");
            return User::where('email', $email)->first();
        }

        return response()->json([
            'status' => 0,
            'message' => 'Invalid: Email or Password!'
        ], 401);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'name' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 0,
                'message' => 'Something went wrong!',
                'error' => $validator->errors()->first()
            ], 400);
        }

        $email = $request->email;
        $password = $request->password;
        $name = $request->name;

        // SQL: $pdo->query("insert into `users` (`name`, `email`, `password`, `created_at`, `updated_at`) values ('$name', '$email', '$bcrypt_password')");
        User::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password)
        ]);

        return response()->json([
            'status' => 1,
            'message' => 'Success: User has been registered!'
        ]);
    }
}
