<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'Api\UserController@login');
Route::post('/register', 'Api\UserController@register');



Route::group(['middleware' => ['auth:api']], function () {

    Route::get('/profile', function (Request $request) {
        return $request->user();
    });

    Route::get('/friend/add/{friend_id}', 'Api\FriendController@addFriend');
    Route::get('/friends/remove/{friend_id}', 'Api\FriendController@removeFriend');
    Route::get('/friends', 'Api\FriendController@friends');
    Route::get('/requests', 'Api\FriendController@requests');

    Route::get('/chats', 'Api\ChatController@chats');
    Route::get('/chats/messages/{friend_id}/{last_id?}', 'Api\ChatController@messages');
    Route::post('/chats/add/{friend_id}', 'Api\ChatController@addMessage');
    Route::post('/chats/say-hi', 'Api\ChatController@say_hi');

});